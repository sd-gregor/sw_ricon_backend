<?php

namespace RiconKonfigurator;

use Doctrine\ORM\Tools\SchemaTool;

use RiconKonfigurator\Models\RI_Vorlage;
use RiconKonfigurator\Models\RI_Eigenschaften;
use RiconKonfigurator\Models\RI_Eigenschaft;
use RiconKonfigurator\Models\RI_Material;

use Shopware\Bundle\AttributeBundle\Service\CrudService;
use Shopware\Bundle\AttributeBundle\Service\TypeMapping;
use Shopware\Components\Model\ModelManager;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UninstallContext;


class RiconKonfigurator extends \Shopware\Components\Plugin
{
    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PreDispatch_Frontend' => 'preparePlugin',
            'Enlight_Controller_Action_PreDispatch_Backend' => 'preparePlugin',
        ];
    }

    public function preparePlugin()
    {
        $this->container->get('Template')->addTemplateDir(
            $this->getPath() . '/Resources/Views/'
        );
        return $this->getPath() . '/Controllers/Frontend/RiconKonfigurator.php';
    }

    public function install(InstallContext $context)
    {
        $this->updateSchema();
        $this->createDemoData();
    }

    public function uninstall(UninstallContext $context)
    {
        /** @var ModelManager $entityManager */
        $entityManager = $this->container->get('models');

        $tool = new SchemaTool($entityManager);

        $classMetaData = [
            $entityManager->getClassMetadata(RI_Vorlage::class),
            $entityManager->getClassMetadata(RI_Eigenschaften::class),
            $entityManager->getClassMetadata(RI_Eigenschaft::class),
            $entityManager->getClassMetadata(RI_Material::class),
        ];

        $tool->dropSchema($classMetaData);
    }

    private function updateSchema()
    {
        /** @var ModelManager $entityManager */
        $entityManager = $this->container->get('models');

        $tool = new SchemaTool($entityManager);

        $classMetaData = [
            $entityManager->getClassMetadata(RI_Vorlage::class),
            $entityManager->getClassMetadata(RI_Eigenschaften::class),
            $entityManager->getClassMetadata(RI_Eigenschaft::class),
            $entityManager->getClassMetadata(RI_Material::class),
        ];

        $tool->createSchema($classMetaData);
    }

    private function createDemoData()
    {
        /** @var ModelManager $entityManager */
        $entityManager = $this->container->get('models');

        $template = new RI_Vorlage();
        $template->setName('Vorlage');
        $template->setEig_hoehe(10);
        $template->setEig_breite(15);
        $template->setEig_tiefe(20);
        $template->setEig_material1(true);
        $template->setEig_material2(true);
        $template->setEig_material3(true);
        $entityManager->persist($template);
        $entityManager->flush($template);

        $eigenschaft = new RI_Eigenschaft();
        $eigenschaft->setBeschreibung('Beschreibung');
        $eigenschaft->setBild('399');
        $entityManager->persist($eigenschaft);
        $entityManager->flush($eigenschaft);

        $eigenschaften = new RI_Eigenschaften();
        $eigenschaften->setName('Testeigenschaften');
        $eigenschaften->setBild('400');
        $entityManager->persist($eigenschaften);
        $entityManager->flush($eigenschaften);

        $material = new RI_Material();
        $material->setName('Holz');
        $material->setInfo('Hier steht die Beschreibung für das Material.');
        $material->setBild('461');
        $material->setPreis(1.25);
        $entityManager->persist($material);
        $entityManager->flush($material);


    }
}
