
Ext.define('Shopware.apps.RiconMaterialliste.model.RI_Material', {
    extend: 'Shopware.data.Model',
    alias:  'widget.riconmaterialliste-model-ri_material',

    configure: function() {
        return {
            controller: 'RiconMaterialliste',
            detail:  'Shopware.apps.RiconMaterialliste.view.detail.Container',
        };
    },
    fields: [
           { name : 'id', type: 'int', useNull: true },
           { name : 'name', type: 'string', useNull: false },
           { name : 'info', type: 'string', useNull: false },
           { name : 'preis', type: 'float', useNull: false },
           { name : 'bild', type: 'string', useNull: false },
           ],

         associations: [{
             relation: 'ManyToOne',
             field: 'bild',
             type: 'hasMany',
             model: 'Shopware.apps.Base.model.Media',
             name: 'getMedia',
             associationKey: 'media'
       }],

       bild: {
           xtype: 'shopware-media-field',
           fieldLabel: '{s name=bild}Bild{/s}',
           valueField: 'path',
           allowBlank: false
       },
  });
