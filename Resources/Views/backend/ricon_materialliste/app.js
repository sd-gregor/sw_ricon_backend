Ext.define('Shopware.apps.RiconMaterialliste', {
    extend: 'Enlight.app.SubApplication',
    alias:  'widget.riconmaterialliste-app',
    name:'Shopware.apps.RiconMaterialliste',

    loadPath: '{url action=load}',
    bulkLoad: true,

    controllers: [ 'Main' ],

    views: [
        'list.Window',
        'list.List',

        'detail.Container',
        'detail.Window'
    ],

    models: [ 'RI_Material' ],
    stores: [ 'RI_Material' ],

    launch: function() {
        return this.getController('Main').mainWindow;
    }
});
