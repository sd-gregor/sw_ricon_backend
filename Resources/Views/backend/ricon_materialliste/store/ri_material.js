
Ext.define('Shopware.apps.RiconMaterialliste.store.RI_Material', {
    extend:'Shopware.store.Listing',
    alias:  'widget.riconmaterialliste-store-ri_material',
    configure: function() {
        return {
            controller: 'RiconMaterialliste'
        };
    },
    model: 'Shopware.apps.RiconMaterialliste.model.RI_Material'
});
