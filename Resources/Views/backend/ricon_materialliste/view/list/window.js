
Ext.define('Shopware.apps.RiconMaterialliste.view.list.Window', {
    extend: 'Shopware.window.Listing',
    alias: 'widget.riconmaterialliste-list-window',
    height: 450,
    title : 'RiconMaterialliste',

    configure: function() {
        return {
            listingGrid: 'Shopware.apps.RiconMaterialliste.view.list.List',
            listingStore: 'Shopware.apps.RiconMaterialliste.store.RI_Material'
        };
    }
});
