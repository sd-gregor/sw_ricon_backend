Ext.define('Shopware.apps.RiconMaterialliste.view.list.List', {
    extend: 'Shopware.grid.Panel',
    alias:  'widget.riconmaterialliste-listing-grid',
    region: 'center',

    configure: function() {
        return {
          /*
            columns: {
                //allowHtml: true,
                //renderer: 'htmlEncode',
            },

            //allowHtml: true,
            */
            detailWindow: 'Shopware.apps.RiconMaterialliste.view.detail.Window'
        };
    }
});
