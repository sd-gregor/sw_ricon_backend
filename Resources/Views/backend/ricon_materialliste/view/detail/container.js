
Ext.define('Shopware.apps.RiconMaterialliste.view.detail.Container', {
    extend: 'Shopware.model.Container',
    padding: 20,

    configure: function() {
     return {
         controller: 'RiconMaterialliste',
         fieldSets: [{
           title: 'Materialdaten',
             fields: {
                 name: {},
                 info: {},
                 preis: {}
             }
         }, {
             title: 'Materialbild',
             layout: 'fit',
             fields: {
                 bild: {},
             }
         }]
     };
 }

});
