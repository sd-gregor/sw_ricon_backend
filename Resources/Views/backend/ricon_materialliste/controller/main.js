Ext.define('Shopware.apps.RiconMaterialliste.controller.Main', {
    extend: 'Enlight.app.Controller',
    alias:  'widget.riconmaterialliste-controller-main',
    init: function() {
        var me = this;
        me.mainWindow = me.getView('list.Window').create({ }).show();
    }
});
