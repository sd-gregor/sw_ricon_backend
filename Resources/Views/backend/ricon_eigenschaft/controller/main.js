Ext.define('Shopware.apps.RiconEigenschaft.controller.Main', {
    extend: 'Enlight.app.Controller',
    alias:  'widget.riconeigenschaft-controller-main',
    init: function() {
        var me = this;
        me.mainWindow = me.getView('list.Window').create({ }).show();
    }
});
