
Ext.define('Shopware.apps.RiconEigenschaft.model.RI_Eigenschaft', {
    extend: 'Shopware.data.Model',
    alias:  'widget.riconeigenschaft-model-ri_eigenschaft',

    configure: function() {
        return {
            controller: 'RiconEigenschaft',
            detail:  'Shopware.apps.RiconEigenschaft.view.detail.Container',
            //related: 'Shopware.apps.RiconEigenschaft.view.detail.RI_Eigenschaft'
        };
    },


    fields: [
        { name : 'id', type: 'int', useNull: true },
        { name : 'beschreibung', type: 'string', useNull: false },
        { name : 'bild', type: 'string', useNull: false },
    ],

    associations: [{
          relation: 'ManyToOne',
          field: 'bild',
          type: 'hasMany',
          model: 'Shopware.apps.Base.model.Media',
          name: 'getMedia',
          associationKey: 'media'
      }],

  bild: {
      xtype: 'shopware-media-field',
      fieldLabel: '{s name=bild}Bild{/s}',
      valueField: 'path',
      allowBlank: false
  },
  });
