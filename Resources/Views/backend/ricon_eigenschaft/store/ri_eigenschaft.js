
Ext.define('Shopware.apps.RiconEigenschaft.store.RI_Eigenschaft', {
    extend:'Shopware.store.Listing',
    alias:  'widget.riconeigenschaft-store-ri_eigenschaft',
    configure: function() {
        return {
            controller: 'RiconEigenschaft'
        };
    },
    model: 'Shopware.apps.RiconEigenschaft.model.RI_Eigenschaft'
});
