Ext.define('Shopware.apps.RiconEigenschaft', {
    extend: 'Enlight.app.SubApplication',
    alias:  'widget.riconeigenschaft-app',
    name:'Shopware.apps.RiconEigenschaft',

    loadPath: '{url action=load}',
    bulkLoad: true,

    controllers: [ 'Main' ],

    views: [
        'list.Window',
        'list.List',

        'detail.Container',
        'detail.Window'
    ],

    models: [ 'RI_Eigenschaft' ],
    stores: [ 'RI_Eigenschaft' ],

    launch: function() {
        return this.getController('Main').mainWindow;
    }
});
