
Ext.define('Shopware.apps.RiconEigenschaft.view.list.Window', {
    extend: 'Shopware.window.Listing',
    alias: 'widget.riconeigenschaft-list-window',
    height: 450,
    title : 'RiconEigenschaft',

    configure: function() {
        return {
            listingGrid: 'Shopware.apps.RiconEigenschaft.view.list.List',
            listingStore: 'Shopware.apps.RiconEigenschaft.store.RI_Eigenschaft'
        };
    }
});
