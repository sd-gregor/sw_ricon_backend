Ext.define('Shopware.apps.RiconEigenschaft.view.list.List', {
    extend: 'Shopware.grid.Panel',
    alias:  'widget.riconeigenschaft-listing-grid',
    region: 'center',

    configure: function() {
        return {
            detailWindow: 'Shopware.apps.RiconEigenschaft.view.detail.Window'
        };
    }
});
