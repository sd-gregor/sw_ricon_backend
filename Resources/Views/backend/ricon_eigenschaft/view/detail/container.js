
Ext.define('Shopware.apps.RiconEigenschaft.view.detail.Container', {
    extend: 'Shopware.model.Container',
    padding: 20,
    /*
    configure: function() {
        return {
        };
    }
    */
    configure: function() {
     return {
         controller: 'RiconEigenschaft',
         fieldSets: [{
           title: 'Eigenschaft',
           layout: 'fit',
             fields: {
               beschreibung: {},
             }
           },{
             title: 'Eigenschaft Bild',
             layout: 'fit',
             fields:{
                 bild: {},
             }
         }]
     };
 }

});
