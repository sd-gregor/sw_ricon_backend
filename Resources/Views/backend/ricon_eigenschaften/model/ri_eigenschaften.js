
Ext.define('Shopware.apps.RiconEigenschaften.model.RI_Eigenschaften', {
    extend: 'Shopware.data.Model',
    alias:  'widget.riconeigenschaften-model-ri_eigenschaften',

    configure: function() {
        return {
            controller: 'RiconEigenschaften',
            detail:  'Shopware.apps.RiconEigenschaften.view.detail.Container',
            //related: 'Shopware.apps.RiconEigenschaften.view.detail.RI_Eigenschaft'
            //listing: 'Shopware.apps.RiconEigenschaften.view.detail.RI_Eigenschaft'
        };
    },


    fields: [
        { name : 'id', type: 'int', useNull: true },
        { name : 'eig_name', type: 'string', useNull: false },
        { name : 'bild', type: 'string', useNull: false },
      //  { name : 'eigenschaft', type: 'auto', useNull: false },
    ],

    associations: [{
          relation: 'ManyToOne',
          field: 'bild',
          type: 'hasMany',
          model: 'Shopware.apps.Base.model.Media',
          name: 'getMedia',
          associationKey: 'media'
      },/*{
          relation: 'ManyToMany',
          field:  'mytemplate',
          type: 'hasMany',
          model: 'Shopware.apps.MytemplateEigen.model.Mytemplate',
          name: 'getMytemplate',
          associationKey: 'mytemplate'
      },*/{
          relation: 'ManyToMany',
          //relation: 'OneToMany',
          //storeClass: 'Shopware.apps.RiconEigenschaften.store.RI_Eigenschaft',
          //lazyLoading: true,
          field:  'eigenschaft',
          type: 'hasMany',
          model: 'Shopware.apps.RiconEigenschaften.model.RI_Eigenschaft',
          name: 'getEigenschaft',
          associationKey: 'eigenschaft'
      }],

  bild: {
      xtype: 'shopware-media-field',
      fieldLabel: '{s name=bild}Bild{/s}',
      valueField: 'path',
      allowBlank: false
  }/*,
  eigenschaft: {
      xtype: 'field',
      fieldLabel: '{s name=eigenschaft}Eigenschaft{/s}',
      valueField: 'id',
      allowBlank: false
  }*/
  });
