
Ext.define('Shopware.apps.RiconEigenschaften.model.RI_Eigenschaft', {
    extend: 'Shopware.data.Model',
    alias:  'widget.riconeigenschaften-model-ri_eigenschaft',

    configure: function() {
        return {
            controller: 'RiconEigenschaften',
            detail:  'Shopware.apps.RiconEigenschaften.view.detail.Container',
            //listing: 'Shopware.apps.RiconEigenschaften.view.detail.RI_Eigenschaft',
            related: 'Shopware.apps.RiconEigenschaften.view.detail.RI_Eigenschaft',
        };
    },


    fields: [
        { name : 'id', type: 'int', useNull: true },
        { name : 'beschreibung', type: 'string', useNull: false },
        { name : 'bild', type: 'string', useNull: false },
        //{ name : 'eigenschaften', type: 'auto', useNull: false },
    ],

    associations: [{
          relation: 'ManyToOne',
          field: 'bild',
          type: 'hasMany',
          model: 'Shopware.apps.Base.model.Media',
          name: 'getMedia',
          associationKey: 'media'
      },/*{
          relation: 'ManyToMany',
          field:  'mytemplate',
          type: 'hasMany',
          model: 'Shopware.apps.MytemplateEigen.model.Mytemplate',
          name: 'getMytemplate',
          associationKey: 'mytemplate'
      },*/{
          //relation: 'ManyToOne'
          relation: 'ManyToMany',
          field:  'eigenschaften',
          type: 'hasMany',
          model: 'Shopware.apps.RiconEigenschaften.model.RI_Eigenschaften',
          name: 'getEigenschaft',
          associationKey: 'eigenschaft'
      }],

  bild: {
      xtype: 'shopware-media-field',
      fieldLabel: '{s name=bild}Bild{/s}',
      valueField: 'path',
      allowBlank: false,
  },/*
  eigenschaften: {
     xtype: 'grid',
     fieldLabel: '{s name=eigenschaften}Eigenschaften{/s}',
     valueField: 'id',
     allowBlank: false,
  }*/
  });
