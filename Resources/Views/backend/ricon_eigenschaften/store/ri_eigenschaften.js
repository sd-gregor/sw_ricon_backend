
Ext.define('Shopware.apps.RiconEigenschaften.store.RI_Eigenschaften', {
    extend:'Shopware.store.Listing',
    alias:  'widget.riconeigenschaften-store-ri_eigenschaften',
    configure: function() {
        return {
            controller: 'RiconEigenschaften'
        };
    },
    model: 'Shopware.apps.RiconEigenschaften.model.RI_Eigenschaften'
});
