Ext.define('Shopware.apps.RiconEigenschaften.store.RI_Eigenschaft', {
    extend: 'Shopware.store.Association',
    model: 'Shopware.apps.RiconEigenschaften.model.RI_Eigenschaft',
    configure: function() {
        return {
            controller: 'RiconEigenschaften',
            proxy:{
               type:'ajax',
               url:'{url controller=RiconEigenschaften action="loadEigenschaft"}',
               reader:{
                   type:'json',
                   root:'data',
                   totalProperty:'total'
               }
           }
        };
    }
});
