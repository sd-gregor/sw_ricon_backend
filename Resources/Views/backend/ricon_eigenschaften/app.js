Ext.define('Shopware.apps.RiconEigenschaften', {
    extend: 'Enlight.app.SubApplication',
    alias:  'widget.riconeigenschaften-app',
    name:'Shopware.apps.RiconEigenschaften',

    loadPath: '{url action=load}',
    bulkLoad: true,

    controllers: [ 'Main' ],

    views: [
        'list.Window',
        'list.List',

        'detail.Container',
        'detail.Window'
    ],

    models: [ 'RI_Eigenschaften', 'RI_Eigenschaft'],
    stores: [ 'RI_Eigenschaften' ],

    launch: function() {
        return this.getController('Main').mainWindow;
    }
});
