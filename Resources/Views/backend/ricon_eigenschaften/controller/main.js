Ext.define('Shopware.apps.RiconEigenschaften.controller.Main', {
    extend: 'Enlight.app.Controller',
    alias:  'widget.riconeigenschaften-controller-main',
    init: function() {
        var me = this;
        me.mainWindow = me.getView('list.Window').create({ }).show();
    }
});
