Ext.define('Shopware.apps.RiconEigenschaften.view.detail.Container', {
    extend: 'Shopware.model.Container',
    padding: 20,
    /*
    configure: function() {
        return {
        };
    }
    */
    configure: function() {
     return {
         controller: 'RiconEigenschaften',
         associations: [ 'eigenschaft' ],
         fieldSets: [{
           title: 'Eigenschaften',
           layout: 'fit',
             fields: {
               eig_name: {},
             }
           },{
             title: 'Eigenschaften Bild',
             layout: 'fit',
             fields:{
                 bild: {},
             }
         }/*,{
           title: 'Eigenschaft',
           layout: 'fit',
           fields:{
               eigenschaft: {},
           }
       }*/]
     };
 }

});
