Ext.define('Shopware.apps.RiconEigenschaften.view.detail.RI_Eigenschaft', {
	extend: 'Shopware.grid.Association',
	//extend: 'Shopware.grid.Panel',
	alias: 'widget.riconeigenschaften-detail-ri_eigenschaft',
	height: 250,
	title: 'Eigenschaft',

	configure: function() {
		var me = this;
		return {
			controller: 'RiconEigenschaften',
			columns:
			{
				    beschreibung: {},
			}
		};
	},
});
/*
Ext.define('Shopware.apps.RiconEigenschaften.view.detail.RI_Eigenschaft', {
    extend: 'Shopware.grid.Panel',
    alias: 'widget.riconeigenschaften-detail-ri_eigenschaft',
    title: 'Eigenschaft',
    height: 300,

    configure: function() {
		var me = this;
		return {
			controller: 'RiconEigenschaften',
			columns:
			{
				beschribung: {},
			}
		};
	}
});
*/
