
Ext.define('Shopware.apps.RiconEigenschaften.view.list.Window', {
    extend: 'Shopware.window.Listing',
    alias: 'widget.riconeigenschaften-list-window',
    height: 450,
    title : 'RiconEigenschaften',

    configure: function() {
        return {
            listingGrid: 'Shopware.apps.RiconEigenschaften.view.list.List',
            listingStore: 'Shopware.apps.RiconEigenschaften.store.RI_Eigenschaften'
        };
    }
});
