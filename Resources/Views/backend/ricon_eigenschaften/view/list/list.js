Ext.define('Shopware.apps.RiconEigenschaften.view.list.List', {
    extend: 'Shopware.grid.Panel',
    alias:  'widget.riconeigenschaften-listing-grid',
    region: 'center',

    configure: function() {
        return {
          /*
            columns: {
                //allowHtml: true,
                //renderer: 'htmlEncode',
            },

            //allowHtml: true,
            */
            detailWindow: 'Shopware.apps.RiconEigenschaften.view.detail.Window'
        };
    }
});
