
Ext.define('Shopware.apps.RiconKonfigurator.model.RI_Vorlage', {
    extend: 'Shopware.data.Model',
    alias:  'widget.riconkonfigurator-model-ri_vorlage',
    configure: function() {
        return {
            controller: 'RiconKonfigurator',
            detail: 'Shopware.apps.RiconKonfigurator.view.detail.Container'
        };
    },
    fields: [
        { name : 'id', type: 'int', useNull: true },
        { name : 'name', type: 'string', useNull: false },
        { name : 'eig_hoehe', type: 'float', useNull: false },
        { name : 'eig_breite', type: 'float', useNull: false },
        { name : 'eig_tiefe', type: 'float', useNull: false },
        { name : 'eig_material1', type: 'boolean', useNull: false },
        { name : 'eig_material2', type: 'boolean', useNull: false },
        { name : 'eig_material3', type: 'boolean', useNull: false },
        ],

    associations: [{
        relation: 'ManyToMany',
        field: 'eigenschaften',
        type: 'hasMany',
        model: 'Shopware.apps.RiconKonfigurator.model.RI_Eigenschaften',
        name: 'getEigenschaften',
        associationKey: 'eigenschaften'
    }]
});
