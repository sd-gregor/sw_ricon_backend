
Ext.define('Shopware.apps.RiconKonfigurator.model.RI_Eigenschaften', {
    extend: 'Shopware.data.Model',
    alias:  'widget.riconkonfigurator-model-ri_eigenschaften',

    configure: function() {
        return {
            controller: 'RiconKonfigurator',
            detail:  'Shopware.apps.RiconKonfigurator.view.detail.Container',
            related: 'Shopware.apps.RiconKonfigurator.view.detail.RI_Eigenschaften'
        };
    },


    fields: [
        { name : 'id', type: 'int', useNull: true },
        { name : 'eig_name', type: 'string', useNull: false },
        { name : 'bild', type: 'string', useNull: false },
    ],

    associations: [{
          relation: 'ManyToOne',
          field: 'bild',
          type: 'hasMany',
          model: 'Shopware.apps.Base.model.Media',
          name: 'getMedia',
          associationKey: 'media'
      },{
          relation: 'ManyToMany',
          field:  'mytemplate',
          type: 'hasMany',
          model: 'Shopware.apps.RiconKonfigurator.model.RI_Vorlage',
          name: 'getMytemplate',
          associationKey: 'mytemplate'
      },/*{
          relation: 'OneToMany',
          field:  'eigenschaft',
          type: 'hasMany',
          model: 'Shopware.apps.RiconKonfigurator.model.RI_Eigenschaft',
          name: 'getEigenschaft',
          associationKey: 'eigenschaft'
      }*/],

  bild: {
      xtype: 'shopware-media-field',
      fieldLabel: '{s name=bild}Bild{/s}',
      valueField: 'path',
      allowBlank: false
  },
  /*eig_name: {
      xtype: 'textarea',
      fieldLabel: '{s name=eig_name}eig_name{/s}',
      valueField: 'path',
      allowBlank: false
  }*/
  });
