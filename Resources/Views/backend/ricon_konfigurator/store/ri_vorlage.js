
Ext.define('Shopware.apps.RiconKonfigurator.store.RI_Vorlage', {
    extend:'Shopware.store.Listing',
    alias:  'widget.riconkonfigurator-store-ri_vorlage',
    configure: function() {
        return {
            controller: 'RiconKonfigurator'
        };
    },
    model: 'Shopware.apps.RiconKonfigurator.model.RI_Vorlage'
});
