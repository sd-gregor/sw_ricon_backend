Ext.define('Shopware.apps.RiconKonfigurator', {
    extend: 'Enlight.app.SubApplication',
    alias:  'widget.riconkonfigurator-app',
    name:'Shopware.apps.RiconKonfigurator',

    loadPath: '{url action=load}',
    bulkLoad: true,

    controllers: [ 'Main' ],

    views: [
        'list.Window',
        'list.List',

        'detail.Container',
        'detail.Window',
        'detail.RI_Eigenschaften'
    ],

    models: [ 'RI_Vorlage' , 'RI_Eigenschaften'],
    stores: [ 'RI_Vorlage' ],

    launch: function() {
        return this.getController('Main').mainWindow;
    }
});
