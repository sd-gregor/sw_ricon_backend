
Ext.define('Shopware.apps.RiconKonfigurator.view.detail.Container', {
    extend: 'Shopware.model.Container',
    padding: 20,
    /*
    configure: function() {
        return {
        };
    }
    */
    configure: function() {
     return {
         controller: 'RiconKonfigurator',
         associations: [ 'eigenschaften' ],
         fieldSets: [{
           title: 'RiconKonfigurator_Name',
           layout: 'fit',
             fields: {
                 name: {},
             }
         },{
           title: 'RiconKonfigurator_Eigenschaften',
             fields: {
                 eig_material1: {},
                 eig_material2: {},
                 eig_material3: {},
                 eig_hoehe: {},
                 eig_breite: {},
                 eig_tiefe: {}
             }
         }]
     };
 }

});
