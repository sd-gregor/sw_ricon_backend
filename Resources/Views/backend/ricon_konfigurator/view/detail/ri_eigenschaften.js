Ext.define('Shopware.apps.RiconKonfigurator.view.detail.RI_Eigenschaften', {
	extend: 'Shopware.grid.Association',
	alias: 'widget.riconkonfigurator-detail-ri_eigenschaften',
	height: 250,
	title: 'Eigenschaften',

	configure: function() {
		var me = this;
		return {
			controller: 'RiconKonfigurator',
			columns:
			{
				eig_name: {},
			}
		};
	},
});
