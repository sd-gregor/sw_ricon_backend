
Ext.define('Shopware.apps.RiconKonfigurator.view.detail.Window', {
    extend: 'Shopware.window.Detail',
    alias: 'widget.riconkonfigurator-detail-window',

    title : 'RiconKonfigurator',
    height: 600,
    width: 900
});
