Ext.define('Shopware.apps.RiconKonfigurator.view.list.List', {
    extend: 'Shopware.grid.Panel',
    alias:  'widget.riconkonfigurator-listing-grid',
    region: 'center',

    configure: function() {
        return {
          /*
            columns: {
                //allowHtml: true,
                //renderer: 'htmlEncode',
            },

            //allowHtml: true,
            */
            detailWindow: 'Shopware.apps.RiconKonfigurator.view.detail.Window'
        };
    }
});
