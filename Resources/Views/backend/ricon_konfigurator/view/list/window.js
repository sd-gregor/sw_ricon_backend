
Ext.define('Shopware.apps.RiconKonfigurator.view.list.Window', {
    extend: 'Shopware.window.Listing',
    alias: 'widget.riconkonfigurator-list-window',
    height: 450,
    title : 'RiconKonfigurator',

    configure: function() {
        return {
            listingGrid: 'Shopware.apps.RiconKonfigurator.view.list.List',
            listingStore: 'Shopware.apps.RiconKonfigurator.store.RI_Vorlage'
        };
    }
});
