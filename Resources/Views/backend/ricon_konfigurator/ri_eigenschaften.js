Ext.define('Shopware.apps.RiconEigenschaften.view.detail.RI_Eigenschaft', {
	extend: 'Shopware.grid.Association',
	alias: 'widget.riconeigenschaften-detail-ri_eigenschaft',
	height: 250,
	title: 'Eigenschaft',

	configure: function() {
		var me = this;
		return {
			controller: 'RiconEigenschaften',
			columns:
			{
				beschreibung: {},
			}
		};
	},
});
