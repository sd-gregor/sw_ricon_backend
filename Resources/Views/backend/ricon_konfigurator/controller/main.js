Ext.define('Shopware.apps.RiconKonfigurator.controller.Main', {
    extend: 'Enlight.app.Controller',
    alias:  'widget.riconkonfigurator-controller-main',
    init: function() {
        var me = this;
        me.mainWindow = me.getView('list.Window').create({ }).show();
    }
});
