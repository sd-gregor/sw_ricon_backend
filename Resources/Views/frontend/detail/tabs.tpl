{*extends file='parent:frontend/detail/tabs.tpl'}

{block name="frontend_detail_tabs_navigation_inner" append}
    <a href="#" class="tab--link" title="Tab" data-tabName="tab">FAQ</a>
{/block}

{block name="frontend_detail_tabs_content_inner" append}
    {if $sArticle.lorem_disable_faq != 1}
        <div class="tab--container">
            {Normal title }
            <div class="tab--header">
                <a href="#" class="tab--title"
                   title="FAQ">FAQ</a>
            </div>

            {* Title for mobile mode }
            <div class="tab--preview">
                <a href="#" class="tab--link">Für die FAQs hier klicken</a>
            </div>

            {*FAQ content}
            <div class="tab--content">
                <div class="content--description">
                    {foreach $lorem_faq as $question}
                        <div class="content--title">
                            {$question.question}
                        </div>
                        <small>Frage von {$question.customer.firstname} {$question.customer.lastname}</small>
                        <div class="product--description">
                            <ul>
                                {foreach $question.answers as $answer}
                                    <li>
                                        {$answer.customer.firstname} {$answer.customer.lastname}: {$answer.answer}
                                    </li>
                                {/foreach}
                            </ul>
                        </div>
                    {/foreach}

                </div>
            </div>
        </div>
    {/if}
{/block*}
