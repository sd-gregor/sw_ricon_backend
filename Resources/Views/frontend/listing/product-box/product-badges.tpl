{*extends file="parent:frontend/listing/product-box/product-badges.tpl"}

{block name="frontend_listing_box_article_discount" prepend}
    {if $sArticle.attributes.lorem_faq && $sArticle.attributes.lorem_faq->get('count') > 0 && $sArticle.lorem_disable_faq != 1}
        <div class="product--badge badge--recommend">
                <i class="icon--question">{$sArticle.attributes.lorem_faq->get('count')}</i>
        </div>
    {/if}
{/block*}
