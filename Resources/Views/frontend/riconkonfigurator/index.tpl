{extends file='parent:frontend/index/index.tpl'}

{block name="frontend_index_header_title"}
    RiconKonfigurator
{/block}

{block name="frontend_index_content"}
            <div class="content--description">
                <h1>Ricon-Konfigurator</h1>
                {foreach $allevorlagen as $vorlagen}
                    <div class="product--description">
                        <ul>Vorlage:
                            {foreach $vorlagen as $vorlage}
                                <li>
                                    {$vorlage}
                                </li>
                            {/foreach}
                        </ul>
                    </div>
                {/foreach}

            </div>
{/block}
 {block name='frontend_index_content_left'}{/block}
{block name="frontend_listing_index_topseller"}{/block}
