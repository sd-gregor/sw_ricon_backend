<?php

namespace RiconKonfigurator\Models;

use Doctrine\ORM\Mapping as ORM;
use Shopware\Components\Model\ModelEntity;

/**
 * @ORM\Entity(repositoryClass="Repository")
 * @ORM\Table(name="ri_material")
 */
class RI_Material extends ModelEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="info", type="string", nullable=false)
     */
    private $info;

    //,options={"default" : "Das Material hat kein Bild"}
    /**
     * @ORM\Column(name="bild", type="string", nullable=false)
     */
    private $bild;

    /**
     * @ORM\Column(name="preis", type="float", nullable=false)
     */
    private $preis;

    public function __construct()
    {
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getInfo()
    {
        return $this->info;
    }

    public function setInfo($info)
    {
        $this->info = $info;
    }

    public function getBild()
    {
        return $this->bild;
    }

    public function setBild($bild)
    {
        $text = '&lt;div title=&quot;Das Material hat kein Bild&quot; class=&quot;sprite-image--exclamation&quot;'
      .'style=&quot;width: 25px; height: 25px; display: inline-block; margin-right: 3px;&quot;&gt;&nbsp;&lt;/div&gt;';

      //  $text = 'Das Material hat kein Bild';
        $this->bild = ($bild === null || $bild === '') ? $text : $bild;
    }

    public function getPreis()
    {
        return $this->preis;
    }

    public function setPreis($preis)
    {
        $this->preis = $preis;
    }
}
