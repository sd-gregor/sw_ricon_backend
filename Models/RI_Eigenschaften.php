<?php

namespace RiconKonfigurator\Models;

use Doctrine\ORM\Mapping as ORM;
use Shopware\Components\Model\ModelEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Repository")
 * @ORM\Table(name="ri_eigenschaften")
 */
class RI_Eigenschaften extends ModelEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id",type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="eig_name", type="string", options={"default" : "leer"})
     */
    private $eig_name;

    /**
     * @ORM\Column(name="bild", type="string")
     */
    private $bild;
/*
    // , orphanRemoval=true, options={"default" : "0"} ArrayCollection
    / **
        * @var RI_Eigenschaft[]
        * @ORM\Column(name="eigenschaft")
        * @ORM\OneToMany(targetEntity="RiconKonfigurator\Models\RI_Eigenschaft",
        * mappedBy="eigenschaften", cascade={"persist", "remove"})
        * /
    private $eigenschaft;
*/
     /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="\RiconKonfigurator\Models\RI_Eigenschaft")
     * @ORM\JoinTable(name="ri_eigenschaften_to_eigenschaft_join",
     *      joinColumns={
     *          @ORM\JoinColumn(
     *              name="eigenschaften_id",
     *              referencedColumnName="id"
     *          )
     *      },
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="eigenschaft_id",
     *              referencedColumnName="id"
     *          )
     *      }
     * )
     */
    protected $eigenschaft;
    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="\RiconKonfigurator\Models\RI_Vorlage")
     */
    protected  $mytemplate;

    //public function __construct()  {  }

    public function __construct()
    {
        $this->eigenschaft = new ArrayCollection();
        $this->mytemplate  = new ArrayCollection();
    }

    /**
      * @param RI_Vorlage $mytemplate
      */
    public function addMytemplate(RI_Vorlage $mytemplate)
    {
        if ($this->mytemplate->contains($mytemplate)) {
            return $mytemplate;
        }
        $this->mytemplate->add($mytemplate);
        $mytemplate->addEigenschaften($this);
    }

    /**
      * @param RI_Vorlage $mytemplate
      */
    public function removeMytemplate(RI_Vorlage $mytemplate)
    {
        if (!$this->mytemplate->contains($mytemplate)) {
            return;
        }
        $this->mytemplate->removeElement($mytemplate);
        $mytemplate->removeEigenschaften($this);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->eig_name;
    }

    public function setName($eig_name)
    {
        $this->eig_name = $eig_name;
    }

    public function getBild()
    {
        return $this->bild;
    }

    public function setBild($bild)
    {
        $text = '&lt;div title=&quot;Das Material hat kein Bild&quot; class=&quot;sprite-image--exclamation&quot;'
        .'style=&quot;width: 25px; height: 25px; display: inline-block; margin-right: 3px;&quot;&gt;&nbsp;&lt;/div&gt;';
          $this->bild = ($bild === null || $bild === '') ? $text : $bild;
    }
     /**
     * @param RI_Eigenschaft $eigenschaft
     */
    public function addEigenschaft(RI_Eigenschaft $eigenschaft)
    {
         if ($this->eigenschaft->contains($eigenschaft)) {
             return $eigenschaft;
         }
         $this->eigenschaft->add($eigenschaft);
         $eigenschaft->addEigenschaften($this);
    }

     /**
      * @param RI_Eigenschaft $eigenschaft
      */
    public function removeEigenschaft(RI_Eigenschaft $eigenschaft)
    {
        if (!$this->eigenschaft->contains($eigenschaft)) {
             return;
         }
         $this->eigenschaft->removeElement($eigenschaft);
         $eigenschaft->removeEigenschaften($this);
  }
/*
    public function getEigenschaft()
    {
       return $this->eigenschaft;
    }

    public function setEigenschaft($eigenschaft)
    {
        return $this->setOneToMany($eigenschaft, '\RiconKonfigurator\Models\RI_Eigenschaft', 'eigenschaft', 'eigenschaften');
    }
    
    public function getEigenschaften()
    {
        return $this;
    }
    */
}
