<?php
namespace RiconKonfigurator\Models;

use Doctrine\ORM\Mapping as ORM;
use Shopware\Components\Model\ModelEntity;

/**
 * @ORM\Entity(repositoryClass="Repository")
 * @ORM\Table(name="ri_eigenschaft")
 */
class RI_Eigenschaft extends ModelEntity
{

  /**
   * @ORM\Id
   * @ORM\Column(name="id", type="integer")
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @ORM\Column(name="bild", type="string")
   */
  private $bild;

  /**
   * @ORM\Column(name="beschreibung", type="string", options={"default" : "leer"})
   */
  private $beschreibung;

/*
//   * @ORM\Column(name="eigenschaften", type="integer", options={"default" : "0"})
  / **
    * @ORM\ManyToOne(targetEntity="RiconKonfigurator\Models\RI_Eigenschaften", inversedBy="eigenschaft")
    * @ORM\JoinColumn(name="eigenschaften", referencedColumnName="id")
    * @var RI_Eigenschaften
    * /
  private $eigenschaften;
*/
    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="\RiconKonfigurator\Models\RI_Eigenschaften")
     */
    protected  $eigenschaften;
    
  //public function __construct()  {     }
  public function __construct()//RI_Eigenschaften $eigenschaften)
  {
      //  $this->eigenschaften = $eigenschaften;
      //  $eigenschaften->setEigenschaft($this);

  }

  public function getId()
  {
      return $this->id;
  }

  public function getBild()
  {
      return $this->bild;
  }

  public function setBild($bild)
  {
      $text = '&lt;div title=&quot;Das Material hat kein Bild&quot; class=&quot;sprite-image--exclamation&quot;'
      .'style=&quot;width: 25px; height: 25px; display: inline-block; margin-right: 3px;&quot;&gt;&nbsp;&lt;/div&gt;';
      $this->bild = ($bild === null || $bild === '') ? $text : $bild;
  }

  public function getBeschreibung()
  {
      return $this->beschreibung;
  }

  public function setBeschreibung($beschreibung)
  {
      $this->beschreibung = $beschreibung;
  }
    
  /**
    * @param RI_Eigenschaften $eigenschaften
    */
  public function addEigenschaften(RI_Eigenschaften $eigenschaften)
  {
        if ($this->eigenschaften->contains($eigenschaften)) {
            return $eigenschaften;
        }
        $this->eigenschaften->add($eigenschaften);
        $eigenschaften->addEigenschaft($this);
    }

    /**
      * @param RI_Eigenschaften $mytemplate
      */
    public function removeEigenschaften(RI_Eigenschaften $eigenschaften)
    {
        if (!$this->eigenschaften->contains($eigenschaften)) {
            return;
        }
        $this->eigenschaften->removeElement($eigenschaften);
        $eigenschaften->removeEigenschaft($this);
    }
/*
  public function getEigenschaften()
  {
      return $this->eigenschaften;
  }

  public function setEigenschaften($eigenschaften)
  {
      $this->eigenschaften = $eigenschaften;
  }
*/
}
