<?php

namespace RiconKonfigurator\Models;

use Doctrine\ORM\Mapping as ORM;
use Shopware\Components\Model\ModelEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Repository")
 * @ORM\Table(name="ri_vorlage")
 */
class RI_Vorlage extends ModelEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="eig_hoehe", type="float")
     */
    private $eig_hoehe;

    /**
     * @ORM\Column(name="eig_breite", type="float")
     */
    private $eig_breite;

    /**
     * @ORM\Column(name="eig_tiefe", type="float")
     */
    private $eig_tiefe;

    /**
     * @ORM\Column(name="eig_material1", type="boolean")
     */
    private $eig_material1;

    /**
     * @ORM\Column(name="eig_material2", type="boolean")
     */
    private $eig_material2;

    /**
     * @ORM\Column(name="eig_material3", type="boolean")
     */
    private $eig_material3;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="\RiconKonfigurator\Models\RI_Eigenschaften")
     * @ORM\JoinTable(name="ri_konfigurator_to_eigenschaften_join",
     *      joinColumns={
     *          @ORM\JoinColumn(
     *              name="mytemplate_id",
     *              referencedColumnName="id"
     *          )
     *      },
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="eigenschaften_id",
     *              referencedColumnName="id"
     *          )
     *      }
     * )
     */
    protected $eigenschaften;

    public function __construct()
    {
            $this->eigenschaften = new ArrayCollection();
    }

    /**
     * @param RI_Eigenschaften $eigenschaften
     */
    public function addEigenschaften(RI_Eigenschaften $eigenschaften)
    {
         if ($this->eigenschaften->contains($eigenschaften)) {
             return $eigenschaften;
         }
         $this->eigenschaften->add($eigenschaften);
         $eigenschaften->addEigenschaften($this);
    }

     /**
      * @param RI_Eigenschaften $eigenschaften
      */
    public function removeEigenschaften(RI_Eigenschaften $eigenschaften)
    {
        if (!$this->eigenschaften->contains($eigenschaften)) {
             return;
         }
         $this->eigenschaften->removeElement($eigenschaften);
         $eigenschaften->removeMytemplate($this);
  }
  /**
   * @var ArrayCollection
   */
   public function getEigenschaften()
   {
       return $this->eigenschaften;
   }

   /**
    * @param \Doctrine\Common\Collections\ArrayCollection $eigenschaften
    */
   public function setEigenschaften($eigenschaften)
   {
       $this->eigenschaften = $eigenschaften;
   }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getEig_hoehe()
    {
        return $this->eig_hoehe;
    }

    public function setEig_hoehe($eig_hoehe)
    {
        $this->eig_hoehe = $eig_hoehe;
    }

    public function getEig_breite()
    {
        return $this->eig_breite;
    }

    public function setEig_breite($eig_breite)
    {
        $this->eig_breite = $eig_breite;
    }

    public function getEig_tiefe()
    {
        return $this->eig_tiefe;
    }

    public function setEig_tiefe($eig_tiefe)
    {
        $this->eig_tiefe = $eig_tiefe;
    }

    public function getEig_material1()
    {
        return $this->eig_material1;
    }

    public function setEig_material1($eig_material1)
    {
        $this->eig_material1 = $eig_material1;
    }

    public function getEig_material2()
    {
        return $this->eig_material2;
    }

    public function setEig_material2($eig_material2)
    {
        $this->eig_material2 = $eig_material2;
    }

    public function getEig_material3()
    {
        return $this->eig_material3;
    }

    public function setEig_material3($eig_material3)
    {
        $this->eig_material3 = $eig_material3;
    }
}
