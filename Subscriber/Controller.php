<?php
namespace RiconKonfigurator\Subscriber;

use Enlight\Event\SubscriberInterface;

class Controller implements SubscriberInterface
{
    public static function getSubscribedEvents()
    {
      return [
          'Enlight_Controller_Dispatcher_ControllerPath_Frontend_MytemplateEigen' => 'onGetControllerPath',
          'Enlight_Controller_Dispatcher_ControllerPath_Backend_MytemplateEigen' => 'onGetControllerPathBackend',
      ];
  }

  public function onGetControllerPathBackend()
  {
      return __DIR__ . '/../Controllers/Backend/RiconKonfigurator.php';
  }

  public function onGetControllerPath()
  {
      return __DIR__ . '/../Controllers/Frontend/RiconKonfigurator.php';
  }

}
