<?php
namespace RiconKonfigurator\Subscriber;

use Enlight\Event\SubscriberInterface;
use RiconKonfigurator\Models\RI_Vorlage;
use Shopware\Components\DependencyInjection\Container;
use Shopware\Components\Model\ModelManager;

class RiconKonfigurator implements SubscriberInterface
{
    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PostDispatchSecure_Frontend_MytemplateEigen' => 'onPostDispatchMytemplateEigen'
        ];
    }

    public function onPostDispatchMytemplateEigen(\Enlight_Event_EventArgs $args)
    {
        /** @var \Shopware_Controllers_Frontend_Detail $detailController */
        $detailController = $args->getSubject();
        $view = $detailController->View();

        /** @var ModelManager $em */
        $em = $this->container->get('models');
        /** @var \MytemplateEigen\Models\Repository $repository */

        $repository = $em->getRepository(RI_Vorlage::class);
        $tempid = $view->getAssign('RI_Vorlage')['id'];
        $query = $repository->getQuestionQuery($tempid);
        $result = $query->getArrayResult();

        //$result =  $repository->findAll();
        $view->assign('template', $result);
    }

}
