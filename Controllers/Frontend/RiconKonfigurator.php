<?php

use Shopware\Components\Compatibility\LegacyStructConverter;

class Shopware_Controllers_Frontend_RiconKonfigurator extends Enlight_Controller_Action
{
    public function indexAction()
    {
      /* * @var \Shopware\Bundle\SearchBundle\ProductSearchInterface $search */
      //$search = $this->get('shopware_search.product_search');
      /* * @var \Shopware\Bundle\StoreFrontBundle\Struct\ProductContextInterface $context */
      //$context = $this->get('shopware_storefront.context_service')->getProductContext();

      /** @var $mapper \Shopware\Components\QueryAliasMapper */
      /*
      $mapper = $this->get('query_alias_mapper');
      $mapper->replaceShortRequestQueries($this->Request());

      $criteria = $this->createListingCriteria($context);

      $searchResult = $search->search($criteria, $context);
      $products = $this->convertToTemplateFormat($searchResult->getProducts());

      $data = array(
          // the products to show
          'sArticles' => $products,
          'criteria' => $criteria,
          // Filters
          'facets' => $searchResult->getFacets(),
          // Current page
          'sPage' => $this->Request()->getParam('sPage', 1),
          // Selectbox: Items per page
          'pageSizes' => [1, 8, 12],
          // Current selection: Items per page
          'sPerPage' => $criteria->getLimit(),
          // Total number of items
          'sNumberArticles' => $searchResult->getTotalCount(),
          // Current sorting mode
          'sSort' => $this->Request()->getParam('sSort', $this->get('config')->get('defaultListingSorting')),
          // standardized form of all selected params
          'shortParameters' => $mapper->getQueryAliases(),
          // Do show items (and not only shopping worlds)
          'showListing' => true
      );
      $this->View()->assign($data);
      */

      /** @var \Shopware\Bundle\StoreFrontBundle\Struct\ProductContextInterface $context */
      $context = $this->get('shopware_storefront.context_service')->getProductContext();
      $criteria = $this->createListingCriteria($context);
      $data = array(
         'criteria' => $criteria,
          // Current page
          //'sPage' => $this->Request()->getParam('sPage', 1),
          // Selectbox: Items per page
          //'pageSizes' => [1, 8, 12],
          // Current selection: Items per page
          'sPerPage' => $criteria->getLimit(),
          // Current sorting mode
          //'sSort' => $this->Request()->getParam('sSort', $this->get('config')->get('defaultListingSorting')),
          // Do show items (and not only shopping worlds)
          //'showListing' => true
      );
      $this->View()->assign($data);
  }

  /**
   * Convert the product structs to template array format
   *
   * @param $products
   * @return array
   */
  private function convertToTemplateFormat($products)
  {
      /** @var LegacyStructConverter $structToTemplateConverter */
      $structToTemplateConverter = $this->get('legacy_struct_converter');

      return $structToTemplateConverter->convertListProductStructList($products);
  }

  /**
   * Create the pre-populated Critera object for our listing
   *
   * @param $context
   * @return \Shopware\Bundle\SearchBundle\Criteria
   */
  private function createListingCriteria($context)
  {
      /** @var \Shopware\Bundle\SearchBundle\StoreFrontCriteriaFactory $criteriaFactory */
      $criteriaFactory = $this->get('shopware_search.store_front_criteria_factory');

      return $criteriaFactory->createListingCriteria($this->Request(), $context);
  }
}
