<?php
use RiconKonfigurator\Models\RI_Eigenschaft;
class Shopware_Controllers_Backend_RiconEigenschaften extends Shopware_Controllers_Backend_Application
{
    protected $model = 'RiconKonfigurator\Models\RI_Eigenschaften';
    protected $alias = 'ri_eigenschaften';

    public function getEigenschaft($id)
    {
      $sql =   $sql =   'SELECT `ri_eigenschaft`.id, `ri_eigenschaft`.beschreibung,`ri_eigenschaft`.bild
                FROM `ri_eigenschaft`
                LEFT JOIN (`ri_eigenschaften`, `ri_eigenschaften_to_eigenschaft_join`)
                ON (`ri_eigenschaften`.id = `ri_eigenschaften_to_eigenschaft_join`.eigenschaften_id
                AND `ri_eigenschaften_to_eigenschaft_join`.eigenschaft_id = `ri_eigenschaft`.id)
                WHERE `ri_eigenschaften`.id = '.$id;
       $result = Shopware()->Db()->fetchAll($sql);
       return $result;
    }

    protected function getAdditionalDetailData(array $data)
    {
        $eigenschaft = $this->getEigenschaft($data['id']);
        $data['eigenschaft']  = $eigenschaft;
        return $data;
    }

  public function loadEigenschaftAction($id)
   {
      /*
       $builder = $this->getManager()->createQueryBuilder();
       $builder->select(array('ri_eigenschaft.id', 'ri_eigenschaft.beschreibung', 'ri_eigenschaft.bild'))
               ->from(RI_Eigenschaft::class,'ri_eigenschaft');
       $res = $builder->getQuery()->getResult();
       */
       $sql = 'SELECT `ri_eigenschaft`.id, `ri_eigenschaft`.beschreibung,`ri_eigenschaft`.bild, `ri_eigenschaften`.`eig_name`, `ri_eigenschaften`.`bild`
                 FROM `ri_eigenschaft`
                 LEFT JOIN (`ri_eigenschaften`)
                 ON (`ri_eigenschaft`.eigenschaften = `ri_eigenschaften`.eigenschaft)
                 WHERE `ri_eigenschaften`.id = '.$id;
      $result = Shopware()->Db()->fetchAll($sql);
       $this->View()->assign(array(
          'success'   => true,
          'data'      => $result
      ));
   }

}
