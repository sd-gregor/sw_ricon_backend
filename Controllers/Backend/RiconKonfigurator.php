<?php

use RiconKonfigurator\Models\RI_Eigenschaften;

class Shopware_Controllers_Backend_RiconKonfigurator extends Shopware_Controllers_Backend_Application
{
    protected $model = 'RiconKonfigurator\Models\RI_Vorlage';
    protected $alias = 'ri_vorlage';

    public function getEigenschaften($mytemplateid)
    {

      $sql =   'SELECT `ri_eigenschaften`.id, `ri_eigenschaften`.eig_name,`ri_eigenschaften`.bild
                FROM `ri_eigenschaften`
                LEFT JOIN (`ri_vorlage`, `ri_konfigurator_to_eigenschaften_join`)
                ON (`ri_vorlage`.id = `ri_konfigurator_to_eigenschaften_join`.mytemplate_id
                AND `ri_konfigurator_to_eigenschaften_join`.eigenschaften_id = `ri_eigenschaften`.id)
                WHERE `ri_vorlage`.id = '.$mytemplateid;
       $result = Shopware()->Db()->fetchAll($sql);
       return $result;
    }

    protected function getAdditionalDetailData(array $data)
    {
        $eigenschaften = $this->getEigenschaften($data['id']);
        $data['eigenschaften']  = $eigenschaften;
        return $data;
    }
}
